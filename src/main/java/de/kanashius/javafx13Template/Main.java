package de.kanashius.javafx13Template;

import de.kanashius.javafx13Template.view.MainView;

public class Main { // This class is not allowed to extend Application and is just used to call the Application
    public static void main(String... args){
        MainView.main(args);
    }
}
