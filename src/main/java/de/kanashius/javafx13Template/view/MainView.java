package de.kanashius.javafx13Template.view;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

// Sample Application
public class MainView extends Application {
    public static void main(String ... args){
        Application.launch(args);
    }
    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setScene(new Scene(new VBox(),800,600));
        WritableImage image=new WritableImage(125,125);
        for(int i=0;i<125;i++)
            for(int j=0;j<125;j++)
                image.getPixelWriter().setColor(i,j, Color.WHITE);
        primaryStage.getIcons().add(image);
        primaryStage.show();
    }
}
